package com.example.bruker.weatherdata;

import java.util.ArrayList;
import java.util.Collections;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridAdapter extends BaseAdapter {

    private ArrayList<WeatherStation> listOfStations = new ArrayList<>();
    LayoutInflater inflater;


    public GridAdapter(Context context, WeatherStation[] stations) {
        Collections.addAll(listOfStations, stations);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listOfStations.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfStations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        WeatherStation chosenStation = (WeatherStation) getItem(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.station_layout, null);

        }

        TextView name = convertView.findViewById(R.id.name);
        TextView coordinates = convertView.findViewById(R.id.position);
        ImageView iv = convertView.findViewById(R.id.iv);

        name.setText(chosenStation.getName());
        coordinates.setText(chosenStation.getPosition());
        iv.setImageBitmap(chosenStation.getImageBmp());

        return convertView;
    }
}