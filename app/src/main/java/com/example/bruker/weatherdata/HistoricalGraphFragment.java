package com.example.bruker.weatherdata;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.eazegraph.lib.charts.ValueLineChart;
import org.eazegraph.lib.models.ValueLinePoint;
import org.eazegraph.lib.models.ValueLineSeries;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class HistoricalGraphFragment extends Fragment {
    ValueLineChart mCubicValueLineChart;
    ValueLineSeries series;
    List<WeatherResults> results;
    List<ValueLinePoint> points = new ArrayList<>();
    ValueLinePoint point;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.graph_fragment, container, false);
        TextView stationNameTv = rootView.findViewById(R.id.stationName);
        TextView stationPositionTv = rootView.findViewById(R.id.stationPosition);

        Bundle args = getArguments();
        stationNameTv.setText(args.getString("name") + " historical");
        stationPositionTv.setText("Position: " + args.getString("pos"));

        DatabaseHelper helper = new DatabaseHelper(getContext());

        results = helper.getAllReadingsById(args.getInt("id"));
        if(results != null) {
            for (WeatherResults result : results) {
                point = new ValueLinePoint(result.getTemperature());
                points.add(point);
            }
        }

        Button temp = rootView.findViewById(R.id.temperature);
        temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateHistoricalSeries(0);
            }
        });

        Button hum = rootView.findViewById(R.id.humidity);
        hum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateHistoricalSeries(1);
            }
        });

        Button pres = rootView.findViewById(R.id.pressure);
        pres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateHistoricalSeries(2);
            }
        });

        mCubicValueLineChart = (ValueLineChart) rootView.findViewById(R.id.cubiclinechart);
        series = new ValueLineSeries();
        series.setColor(0xFF56B7F1);

        if(points != null) {
            series.setSeries(points);
            mCubicValueLineChart.addSeries(series);
        }

        return rootView;
    }

    private void updateHistoricalSeries(int value) {
        points.clear();
        if(results != null) {
            for (WeatherResults result : results) {
                switch (value){
                    case 0:
                        point = new ValueLinePoint(result.getTemperature());
                        break;
                    case 1:
                        point = new ValueLinePoint(result.getHumidity());
                        break;
                    case 2:
                        point = new ValueLinePoint(result.getPressure());
                        break;
                    default:
                        Log.d("updateFailure", "Noe gikk galt!");
                }
                points.add(point);
            }
            series.setSeries(points);
            mCubicValueLineChart.addSeries(series);
        }

    }


}
