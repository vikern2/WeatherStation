package com.example.bruker.weatherdata;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "readings_db";



    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL(WeatherReadingDB.SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + WeatherReadingDB.TABLE_NAME);
        onCreate(db);
    }

    public void emptyDatabase(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(WeatherReadingDB.TABLE_NAME,null,null);
        //String selectQuery = "DELETE  * FROM " + WeatherReadingDB.TABLE_NAME;
        //Cursor cursor = db.rawQuery(selectQuery, null);
        //int count=cursor.getCount();
        //cursor.close();
        //return count;


    }

    public long insertReadings(WeatherResults readings) {




        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(WeatherReadingDB.COLUMN_WS_ID, readings.getId());
        values.put(WeatherReadingDB.COLUMN_TEMPERATURE, readings.getTemperature());
        values.put(WeatherReadingDB.COLUMN_HUMIDITY, readings.getHumidity());
        values.put(WeatherReadingDB.COLUMN_PRESSURE, readings.getPressure());
        values.put(WeatherReadingDB.COLUMN_TIMESTAMP, readings.getTimestamp());

        long id = db.insert(WeatherReadingDB.TABLE_NAME, null, values);
        db.close();
        return id;
    }

    public WeatherResults getReading(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(WeatherReadingDB.TABLE_NAME,
                new String[]{WeatherReadingDB.COLUMN_ID, WeatherReadingDB.COLUMN_WS_ID ,WeatherReadingDB.COLUMN_TEMPERATURE, WeatherReadingDB.COLUMN_HUMIDITY, WeatherReadingDB.COLUMN_PRESSURE, WeatherReadingDB.COLUMN_TIMESTAMP},
                WeatherReadingDB.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        WeatherResults reading = new WeatherResults(
                cursor.getInt(cursor.getColumnIndex(WeatherReadingDB.COLUMN_WS_ID)),
                cursor.getString(cursor.getColumnIndex(WeatherReadingDB.COLUMN_TIMESTAMP)),
                cursor.getFloat(cursor.getColumnIndex(WeatherReadingDB.COLUMN_HUMIDITY)),
                cursor.getFloat(cursor.getColumnIndex(WeatherReadingDB.COLUMN_TEMPERATURE)),
                cursor.getFloat(cursor.getColumnIndex(WeatherReadingDB.COLUMN_PRESSURE)));

        cursor.close();

        return reading;
    }

    public List<WeatherResults> getAllReadings() {
        List<WeatherResults> readings = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + WeatherReadingDB.TABLE_NAME + " ORDER BY " +
                WeatherReadingDB.COLUMN_TIMESTAMP + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                WeatherResults reading = new WeatherResults();
                reading.setId(cursor.getInt(cursor.getColumnIndex(WeatherReadingDB.COLUMN_WS_ID)));
                reading.setTemperature(cursor.getFloat(cursor.getColumnIndex(WeatherReadingDB.COLUMN_TEMPERATURE)));
                reading.setHumidity(cursor.getFloat(cursor.getColumnIndex(WeatherReadingDB.COLUMN_HUMIDITY)));
                reading.setPressure(cursor.getFloat(cursor.getColumnIndex(WeatherReadingDB.COLUMN_PRESSURE)));
                reading.setTimestamp(cursor.getString(cursor.getColumnIndex(WeatherReadingDB.COLUMN_TIMESTAMP)));

                readings.add(reading);
            } while (cursor.moveToNext());
        }

        db.close();

        return readings;
    }

    public List<WeatherResults> getAllReadingsById(int id) {
        List<WeatherResults> readings = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + WeatherReadingDB.TABLE_NAME + " WHERE " + WeatherReadingDB.COLUMN_WS_ID + "='" + id + "'" + " ORDER BY " +
                WeatherReadingDB.COLUMN_TIMESTAMP + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                WeatherResults reading = new WeatherResults();
                reading.setId(cursor.getInt(cursor.getColumnIndex(WeatherReadingDB.COLUMN_WS_ID)));
                reading.setTemperature(cursor.getFloat(cursor.getColumnIndex(WeatherReadingDB.COLUMN_TEMPERATURE)));
                reading.setHumidity(cursor.getFloat(cursor.getColumnIndex(WeatherReadingDB.COLUMN_HUMIDITY)));
                reading.setPressure(cursor.getFloat(cursor.getColumnIndex(WeatherReadingDB.COLUMN_PRESSURE)));
                reading.setTimestamp(cursor.getString(cursor.getColumnIndex(WeatherReadingDB.COLUMN_TIMESTAMP)));

                readings.add(reading);
            } while (cursor.moveToNext());
        }

        db.close();

        return readings;
    }

    public int getReadingsCount() {
        String countQuery = "SELECT  * FROM " + WeatherReadingDB.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    public int getReadingsCountById(int id) {
        String countQuery = "SELECT  * FROM " + WeatherReadingDB.TABLE_NAME + " WHERE " + WeatherReadingDB.COLUMN_WS_ID + "='" + id + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }
}
