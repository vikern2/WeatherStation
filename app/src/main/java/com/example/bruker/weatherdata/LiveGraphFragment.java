package com.example.bruker.weatherdata;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import org.eazegraph.lib.charts.ValueLineChart;
import org.eazegraph.lib.models.ValueLinePoint;
import org.eazegraph.lib.models.ValueLineSeries;

import java.sql.BatchUpdateException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LiveGraphFragment extends Fragment {
    ValueLineChart mCubicValueLineChart;
    ValueLineSeries seriesToDisplay;
    List<ValueLinePoint> humidity = new ArrayList<>();
    List<ValueLinePoint> temperature = new ArrayList<>();
    List<ValueLinePoint> pressure = new ArrayList<>();
    int valueToDisplay = 0;
    TextView maximum;
    TextView minimum;
    TextView current;
    View rootView;
    int id;
    Context context;
    Intent dlById;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.graph_fragment, container, false);

        TextView stationNameTv = rootView.findViewById(R.id.stationName);
        TextView stationPositionTv = rootView.findViewById(R.id.stationPosition);
        maximum = rootView.findViewById(R.id.max);
        minimum = rootView.findViewById(R.id.min);
        current = rootView.findViewById(R.id.current);


        Bundle args = getArguments();
        stationNameTv.setText(args.getString("name") + " live");
        stationPositionTv.setText("Position: " + args.getString("pos"));
        id = args.getInt("id");

        Button temp = rootView.findViewById(R.id.temperature);
        temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valueToDisplay = 0;
            }
        });

        Button hum = rootView.findViewById(R.id.humidity);
        hum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valueToDisplay = 1;
            }
        });

        Button pres = rootView.findViewById(R.id.pressure);
        pres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valueToDisplay = 2;
            }
        });

        final Button startStop = rootView.findViewById(R.id.startStop);
        startStop.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(startStop.getText() == "STOP"){
                    updateStartStop("STOP");
            }else{
                    updateStartStop("START");
                }
        }});

        startStop.setText("STOP");

        mCubicValueLineChart = (ValueLineChart) rootView.findViewById(R.id.cubiclinechart);
        seriesToDisplay = new ValueLineSeries();
        seriesToDisplay.setColor(0xFF000DFF);

        LocalBroadcastManager.getInstance(rootView.getContext()).registerReceiver(
                br, new IntentFilter("newWeatherReport"));

        DownloadService dl = new DownloadService();

        context = rootView.getContext();

        dlById = new Intent(context, DownloadService.class);
        dlById.putExtra("stationId", id);
        context.startService(dlById);

        return rootView;
    }

    public void upd(WeatherResults reading){
        humidity.add(new ValueLinePoint(reading.getHumidity()));
        temperature.add(new ValueLinePoint(reading.getTemperature()));
        pressure.add(new ValueLinePoint(reading.getPressure()));

        switch (valueToDisplay){
            case 0:
                seriesToDisplay.setSeries(temperature);
                seriesToDisplay.setColor(0xFF000DFF);
                current.setText("Current temperature is " + reading.getTemperature());
                break;
            case 1:
                seriesToDisplay.setSeries(humidity);
                current.setText("Current humidity is " + reading.getHumidity());
                seriesToDisplay.setColor(0xFFB90006);
                break;
            case 2:
                seriesToDisplay.setSeries(pressure);
                current.setText("Current pressure is " + reading.getPressure());
                seriesToDisplay.setColor(0xFFF9AD2B);
                break;
            default:
                Toast.makeText(getContext(), "service starting", Toast.LENGTH_SHORT).show();
        }
        mCubicValueLineChart.addSeries(seriesToDisplay);
    }


    private BroadcastReceiver br = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                WeatherResults reading = (WeatherResults) intent.getSerializableExtra("newReading");
                upd(reading);
            }
    };

    private void updateStartStop(String startOrStop) {
        Button startStop = rootView.findViewById(R.id.startStop);
        ImageView light = rootView.findViewById(R.id.light);

        if (Objects.equals(startOrStop, "STOP")) {
            boolean stopService = context.stopService(dlById);
            if(stopService){
                startStop.setText("START");
                light.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            }
        } else if (Objects.equals(startOrStop, "START")) {
            startStop.setText("STOP");
            light.setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_ATOP);
            dlById.putExtra("stationId", id);
            context.startService(dlById);
        }

    }
}
