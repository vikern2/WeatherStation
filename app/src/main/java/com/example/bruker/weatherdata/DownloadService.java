package com.example.bruker.weatherdata;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DownloadService extends Service {
    ScheduledExecutorService executor;
    Thread thread;

    @Override
    public void onCreate(){
        Log.d("a", "created");
    }




    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);
        final String Antall=sharedPreferences.getString("Antall","1000");
        String periode=sharedPreferences.getString("periode","3");

        Log.e("testing",periode);
        Context context = this.getApplicationContext();
        Toast.makeText(context, "service starting", Toast.LENGTH_SHORT).show();
        final String params = String.valueOf(intent.getExtras().getInt("stationId"));
        Log.d("params", params);

        thread = new Thread(new Runnable(){

            @Override
            public void run() {
                try{
                    String stationData = "http://kark.hin.no/~wfa/weather/vdata.php?id=" + params;
                    URL url = new URL(stationData);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                    conn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
                    int responseCode = conn.getResponseCode();

                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        Gson gson = new Gson();
                        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        StringBuilder result = new StringBuilder();
                        String line;
                        while ((line = in.readLine()) != null) {
                            result.append(line);
                        }
                        WeatherResults resultToReturn = gson.fromJson(result.toString(), WeatherResults.class);
                        DatabaseHelper helper = new DatabaseHelper(getBaseContext());

                        if(helper.getReadingsCount()>Integer.parseInt(Antall)) {
                            helper.insertReadings(resultToReturn);
                        }

                        sendLocationBroadcast(resultToReturn);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
        thread.start();

        executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(thread, 0, 20, TimeUnit.SECONDS);

        return START_STICKY;
    }

    private void sendLocationBroadcast(WeatherResults resultToReturn){
        Intent intent = new Intent("newWeatherReport");
        intent.putExtra("newReading", resultToReturn);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        executor.shutdown();
        thread.interrupt();
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
    }
}