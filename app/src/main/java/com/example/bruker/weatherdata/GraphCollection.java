package com.example.bruker.weatherdata;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.WindowManager;

public class GraphCollection extends FragmentActivity {

    GraphPagerAdapter adapter;
    ViewPager vp;
    private int id;
    private String name;
    private String pos;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pager);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        id = extras.getInt("id");
        name = extras.getString("name");
        pos = extras.getString("position");

        adapter = new GraphPagerAdapter(getSupportFragmentManager());
        vp = findViewById(R.id.vp);
        vp.setAdapter(adapter);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    public class GraphPagerAdapter extends FragmentPagerAdapter {
        public GraphPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position){
                case 0:
                    Fragment liveGraph = new LiveGraphFragment();
                    Bundle args = new Bundle();
                    args.putInt("id", id);
                    args.putString("name", name);
                    args.putString("pos", pos);
                    liveGraph.setArguments(args);
                    return liveGraph;
                case 1:
                    Fragment histoGraph = new HistoricalGraphFragment();
                    Bundle argss = new Bundle();
                    argss.putInt("id", id);
                    argss.putString("name", name);
                    argss.putString("pos", pos);
                    histoGraph.setArguments(argss);
                    return histoGraph;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position){
            switch(position){
                case 0:
                    return "Live";
                case 1:
                    return "Historic";
                default:
                    return null;
            }
        }
    }
}
