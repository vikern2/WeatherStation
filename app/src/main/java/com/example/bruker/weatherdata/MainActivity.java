package com.example.bruker.weatherdata;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.Collections;

public class MainActivity extends AppCompatActivity implements DownloadFragment.DownloadCallbacks {
    private static final String FRAGMENT_TAG = "HELPER_FRAGMENT";
    private static final String IMAGE_FRAGMENT_TAG = "IMAGE_HELPER_FRAGMENT";
    private DownloadFragment helperFragment = null;
    private WeatherStation chosenStation;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDrawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);
        mToggle=new ActionBarDrawerToggle(this,mDrawerLayout, R.string.Open, R.string.Close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
        @Override
            public boolean onNavigationItemSelected(MenuItem item){
            item.setChecked(true);
            int id=item.getItemId();
            displaySelectedScreen(id);
            mDrawerLayout.closeDrawers();
            return true;
        }
        });


        FragmentManager fragmentManager = getFragmentManager();
        helperFragment = (DownloadFragment) fragmentManager.findFragmentByTag(FRAGMENT_TAG);

        if (helperFragment == null) {
            helperFragment = new DownloadFragment();
            fragmentManager.beginTransaction().add(helperFragment, FRAGMENT_TAG).commit();
        }

        helperFragment.startDownload();
    }

    private void displaySelectedScreen(int id){
        switch(id){
            case R.id.Om:
                Intent intent1 =new Intent(this,OmActivity.class);
                String TAG="test";
                Log.d(TAG, "displaySelectedScreen: ");
                startActivity(intent1);
                break;
            case R.id.Innstillinger:
                Intent intent2 =new Intent(this,PreferenceWithHeaders.class);
                startActivity(intent2);
                break;
            case R.id.EmptyDatabase:
                Intent intent3 =new Intent(this,EmptyDBActivity.class);
                startActivity(intent3);
                break;
            case R.id.Avslutt:
                int p = android.os.Process.myPid();
                android.os.Process.killProcess(p);
                finish();
                //finishAffinity();
                //System.exit(0);
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(mToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDownloadFinished(WeatherStation[] result) {
        final GridAdapter adapter = new GridAdapter(this, result);
        GridView gv = findViewById(R.id.gv);
        gv.setAdapter(adapter);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                chosenStation = (WeatherStation) adapter.getItem(position);
                showGraph(chosenStation);
            }
        });
        }

    public void showGraph(WeatherStation chosenStation){
        Intent intent = new Intent(this, GraphCollection.class);
        Bundle extras = new Bundle();
        extras.putInt("id", chosenStation.getId());
        extras.putString("name", chosenStation.getName());
        extras.putString("position", chosenStation.getPosition());
        intent.putExtras(extras);
        startActivity(intent);
    }

}

