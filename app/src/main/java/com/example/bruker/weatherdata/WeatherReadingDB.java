package com.example.bruker.weatherdata;
import android.provider.BaseColumns;

public class WeatherReadingDB implements BaseColumns {
        public WeatherReadingDB(){}

        public static final String TABLE_NAME = "readings";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_WS_ID = "stationId";
        public static final String COLUMN_TEMPERATURE = "temperature";
        public static final String COLUMN_HUMIDITY = "humidity";
        public static final String COLUMN_PRESSURE = "pressure";
        public static final String COLUMN_TIMESTAMP = "timestamp";

        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        COLUMN_WS_ID + " TEXT," +
                        COLUMN_TEMPERATURE + " TEXT," +
                        COLUMN_HUMIDITY + " TEXT," +
                        COLUMN_PRESSURE + " TEXT," +
                        COLUMN_TIMESTAMP + " TEXT" +
                        " )";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + TABLE_NAME;


        private int id;
        private int wsId;
        private float temperature;
        private float humidity;
        private float pressure;
        private String timestamp;

    public int getWsId() {
        return wsId;
    }

    public void setWsId(int wsId) {
        this.wsId = wsId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public float getTemperature() {
            return temperature;
        }

        public void setTemperature(float temperature) {
            this.temperature = temperature;
        }

        public float getHumidity() {
            return humidity;
        }

        public void setHumidity(float humidity) {
            this.humidity = humidity;
        }

        public float getPressure() {
            return pressure;
        }

        public void setPressure(float pressure) {
            this.pressure = pressure;
        }

    }


