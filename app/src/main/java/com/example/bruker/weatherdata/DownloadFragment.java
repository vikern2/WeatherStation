package com.example.bruker.weatherdata;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadFragment extends Fragment {

    interface DownloadCallbacks{
        void onDownloadFinished(WeatherStation[] result);
    }

    private DownloadCallbacks callbackActivity = null;


    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        try{
            callbackActivity = (DownloadCallbacks) context;
            startDownload();
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString() + "implementer interface");
        }}



    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onDetach(){
        super.onDetach();
        callbackActivity = null;
    }

    public void startDownload(){
        DownloadAsyncTask downloadAsyncTask = new DownloadAsyncTask();
        downloadAsyncTask.execute();
    }



    public class DownloadAsyncTask extends AsyncTask<Void, WeatherStation[], WeatherStation[]> {
        @Override
        protected WeatherStation[] doInBackground(Void... ignore) {
            try{
                String stationData = "http://kark.hin.no/~wfa/weather/vstations.php";
                StringBuilder serverResponse = new StringBuilder();
                URL url = new URL(stationData);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
                int responseCode = conn.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    Gson gson = new Gson();
                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = in.readLine()) != null) {
                        result.append(line);
                    }
                    WeatherStation[] stationsToReturn = gson.fromJson(result.toString(), WeatherStation[].class);
                    for (WeatherStation ws : stationsToReturn) {
                        try{
                            url = new URL(ws.getImage());
                            conn = (HttpURLConnection) url.openConnection();
                            responseCode = conn.getResponseCode();
                            if(responseCode == HttpURLConnection.HTTP_OK){
                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                                ws.setImageBmp(bmp);
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                    return stationsToReturn;
                } else {
                    try {
                        serverResponse.append("Feilmelding fra server: " + conn.getResponseMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                conn.disconnect();
            }catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(WeatherStation[] result){
            super.onPostExecute(result);
            if(callbackActivity != null)
                callbackActivity.onDownloadFinished(result);
        }
    }
}
