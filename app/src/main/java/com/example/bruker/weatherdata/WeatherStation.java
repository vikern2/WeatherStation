package com.example.bruker.weatherdata;

import android.graphics.Bitmap;

public class WeatherStation {
    int id;
    String name;
    String position;
    String image;
    String thumb;
    Bitmap imageBmp = null;

    public Bitmap getImageBmp() {
        return imageBmp;
    }

    public void setImageBmp(Bitmap imageBmp) {
        this.imageBmp = imageBmp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getImage() {
        return image;
    }

    public void String(String image) {
        this.image = image;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public WeatherStation(int id, String name, String position, String image, String thumb) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.image = image;
        this.thumb = thumb;
    }



}
